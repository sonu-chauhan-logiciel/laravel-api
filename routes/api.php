<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register', 'API\UsersController@register');
Route::post('/login', 'API\LoginController@login');
Route::get('/logout', 'API\LoginController@logout');
Route::get('/user/{id}', 'API\UsersController@show');
Route::put('/user/{id}', 'API\UsersController@update');


Route::group(['middleware' => 'auth:api'], function (){
	Route::get('/registration', 'API\RegistrationController@index');
	Route::get('/users', 'API\UsersController@index');
});
Route::post('/registration/create', 'API\RegistrationController@create');
Route::put('/registration/{id}', 'API\RegistrationController@update');
Route::delete('/registration/{id}', 'API\RegistrationController@delete');

Route::get('/post', 'API\PostController@index');

Route::get('search', 'API\UsersController@getSearch');