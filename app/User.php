<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Traits\Sortable;

class User extends Authenticatable
{
    use Sortable, HasApiTokens, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * set string fields for filtering
     * @var array
     */
    protected $likeFilterFields = ['name', 'email'];

    /**
     * Database Columns
     * @var array
     */
    protected $defaultcolumns = ['id', 'name', 'email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /**
     * Scope a query to search users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeFilter($query, $filters = [])
    {
        foreach ($filters as $field => $value) {
            if(!in_array($field, $this->defaultcolumns)){
                continue;
            }
            if(in_array($field, $this->likeFilterFields)){
                $query->where($field, 'LIKE', "%$value%");
            }else{
                $query->where($field, $value);
            }
        }
        return $query;
    }
}
