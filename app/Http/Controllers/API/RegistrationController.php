<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Registration;
use Validator;

class RegistrationController extends Controller
{
	/** 
	 * User Listing Api 
	 * 
	 * @return \Illuminate\Http\Response 
	*/ 
	public function index(Request $request)
	{
		$data = Registration::get();
        return response()->json(['Register_user' => $data], 200);
	}   

    /** 
     * User Registration Api 
     * 
     * @return \Illuminate\Http\Response 
    */
    public function create(Request $request)
    {
		$validator = Validator::make($request->all(), [ 
						'name' => 'required', 
						'email' => 'required|email', 
						'password' => 'required',  
			     	]);
		if ($validator->fails()) { 
        	return response()->json(['error'=>$validator->errors()], 401);            
    	}
    	$input = $request->all(); 
		$input['password'] = Hash::make($request->input('password'));
		$data = Registration::create($input);
		return response()->json(['Register_user' => $data], 200);
    }

    /** 
     * Register User Update Api 
     * 
     * @return \Illuminate\Http\Response 
    */
    public function update(Request $request, $id)
    {
    	$user = Registration::find($id);
    	$user->update($request->all());
    	/*$input = $request->all();
    	$data = Registration::where('id', $id)->update($input);*/
		return response()->json(['Register_user' => $user], 200);
    }

    /** 
     * Register User Delete Api 
     * 
     * @return \Illuminate\Http\Response 
    */
   	public function delete($id)
   	{
   		$user = Registration::find($id);
   		$user->delete();
		return response()->json(['Message' => 'User Deleted Successfully'], 200);
   	}
}
