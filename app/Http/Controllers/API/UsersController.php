<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Events\UserCreated;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use Validator;
use App\Transformers\UserTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;
use App\User;

class UsersController extends Controller
{
    protected $usersRepository;

    public function __construct(UserRepository $users)
    {
        $this->usersRepository = $users;
    }
    /** 
     * User List api 
     * 
     * @return \Illuminate\Http\Response 
    */
    public function index()
    {
        $users = $this->usersRepository->get();
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        // Make a resource out of the data and
        $resource = new Collection($users, new UserTransformer(), 'user');
        $manager->parseIncludes('post');
        // Run all transformers
        $users = $manager->createData($resource);
        return $users->toArray();
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
    */
   	public function register(Request $request)
   	{   $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required',  
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $user = $this->usersRepository->store($request);
        event(new UserCreated($user));
        return response()->json(['user' => $user], 200);
	}

    /** 
     * View Single User api 
     * 
     * @return \Illuminate\Http\Response 
    */
    public function show($id)
    {
        $user = $this->usersRepository->show($id);
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Item($user, new UserTransformer(), 'user');
        $manager->parseIncludes('post');
        // Run all transformers
        $users = $manager->createData($resource);
        return $users->toArray();
    }

    /** 
     * Update Single User API 
     * 
     * @return \Illuminate\Http\Response 
    */
    public function update(Request $request, $id)
    {
        $user = $this->usersRepository->update($id, $request);
        return response()->json(['user' => $user],200);
    }

    /** 
     * Search Filter API 
     * 
     * @return \Illuminate\Http\Response 
    */
    
    public function getSearch(Request $request)
    {
        $result = $this->usersRepository->search($request);
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        // Make a resource out of the data and
        $resource = new Collection($result, new UserTransformer(), 'user');
        $manager->parseIncludes('post');
        // Run all transformers
        $users = $manager->createData($resource);
        if(count($result)  > 0)
        {
            return $users->toArray();
        }
        else
        {
            return response()->json(['message' => 'Record Not Found'], 404);
        }
    }
}
