<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    
	/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
    */
    public function login(Request $request)
    {
    	$email = $request->input('email');
    	$password = $request->input('password');
    	if (Auth::attempt(['email' => $email, 'password' => $password])) 
    	{
    		$user = Auth::user();
    		$token = $user->createToken($user->email)->accessToken;
    		return response()->json([
    			'user' => $user,
    			'token' => $token
    		], 200);
    	}
    	else
    	{
    		return response()->json([
    			'message' => 'Unauthorized User'
    		], 401);
    	}
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
    */
    public function logout()
    {
        $user = auth()->guard('api')->user();
        if($user)
        {
            Auth::logout();
            return Response(['code' => 200, 'message' => 'You are successfully logged out'], 200);
        }
    }
}
