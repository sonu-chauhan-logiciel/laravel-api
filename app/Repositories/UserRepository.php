<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Post;


class UserRepository 
{
	/** 
	 * User List api 
	 * 
	 * @return \Illuminate\Http\Response 
	*/
	public function get()
	{
		$builder = User::sort('id', 'ASC')->with('posts')->get();
		return $builder;
	}

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
    */
   	public function store(Request $request)
   	{
   		$input = $request->all(); 
   		$password = Hash::make($request->input('password'));
   		$user = User::create([
   			'name' => $request->input('name'),
   			'email' => $request->input('email'),
   			'password' => $password,
   			'status' => $request->input('status'),
   		]);
   		return $user;
	}
	/** 
	 * Show Specific User api 
	 * 
	 * @return \Illuminate\Http\Response 
	*/
	public function show($id)
	{
		$user = User::with('posts')->find($id);
		return $user;
	}

	/** 
	 * Specific User Update API 
	 * 
	 * @return \Illuminate\Http\Response 
	*/
	public function update($id, Request $request)
	{
		$user = User::find($id);
		$result = $user->update($request->all());
		return $result;
	}

	/** 
	 * User Search api 
	 * 
	 * @return \Illuminate\Http\Response 
	*/
	public function search(Request $request)
	{
		$result = User::Filter($request->all())->get();
		return $result;
	}

	/**
	 * Get Users 
	*/
	public function getUsers()
	{
		$users = User::where('status',0)->get();
		return $users;
	}
}