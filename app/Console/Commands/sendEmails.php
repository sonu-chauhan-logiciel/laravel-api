<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SendEmailJob;
use App\Mail\WelcomeMail;

class sendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email to All Registered Users';

    protected $users;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $users)
    {   
        $this->users = $users;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users  = $this->users->getUsers();
        foreach ($users as $user) {
            //dispatch(new SendEmailJob($user));
            SendEmailJob::dispatch($user);
        }
        $this->info('The emails are send successfully!');     
        /*foreach ($users as $user) {
            Mail::to($user->email)->send(new WelcomeMail($user));
            $this->info('The emails are send successfully!');        
        }*/
    }
}
