@component('mail::message')
# Hello {{$user->name}}
Please Click On Accept Button
@component('mail::button', [
    'url' => url('/api/user'.'/'.$user->id)
])
Accept
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent